package simple;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;



import simple.SALES;
import simple.SALEDA;

public class SALES {

	private String saleId;
	private String saleMonth;
	private String saleQuantity;
	private String saleYear;
	private String managerId;
	
	
	public String getSaleYear() {
		return saleYear;
	}

	public void setSaleYear(String saleYear) {
		this.saleYear = saleYear;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	private boolean valid;
	
	public SALES()
	{
		saleId = null;
		saleMonth = null;
		saleYear = null;
		saleQuantity = null;
		managerId = null;
		valid = false;
	}
	
	public SALES(String saleId, String saleMonth, String saleYear, String saleQuantity,String managerId)
	{
		this.saleId = saleId;
		this.saleMonth = saleMonth;
		this.saleYear = saleYear;
		this.saleQuantity = saleQuantity;
		this.managerId = managerId;

	}

	

	public String getSaleId() {
		return saleId;
	}

	public void setSaleId(String saleId) {
		this.saleId = saleId;
	}

	public String getSaleMonth() {
		return saleMonth;
	}

	public void setSaleMonth(String saleMonth) {
		this.saleMonth = saleMonth;
	}

	public String getSaleQuantity() {
		return saleQuantity;
	}

	public void setSaleQuantity(String saleQuantity) {
		this.saleQuantity = saleQuantity;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	public static boolean createSale(String saleId, String saleMonth,String saleYear, String saleQuantity, String managerId)
	{
		boolean valid = SALEDA.createSale(saleId, saleMonth,saleYear, saleQuantity,managerId);
		return valid;
	}
	
	public static List<SALES> listSales()
	{
		List<SALES> sales = new ArrayList<SALES>();
		sales = SALEDA.listSales();
		return sales;
	}
	
	public static void updateSales(String saleId, String saleMonth,String saleYear, String saleQuantity, String managerId)
	{
		SALEDA.updateSales(saleId, saleMonth,saleYear, saleQuantity,managerId);

	}
	
	
	public static SALES viewSales(String manangerId) 
	{
		SALES sales = SALEDA.viewSales(manangerId);
		return sales;
	}
	
	public static void deleteSale(String saleId) 
	{
		System.out.print("bean");
		SALEDA.deleteSale(saleId);

	}
	
	
}
